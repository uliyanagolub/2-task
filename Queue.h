//
// Created by E540 on 01/06/2020.
//

#ifndef INC_2_PAGE_QUEUE_H
#define INC_2_PAGE_QUEUE_H

using namespace std;

class Queue {
private:
    int start_, end_;
    int size_;
    int **buffer_;

public:
    explicit Queue(int size);

    void add(int element);

    int remove();

    int getFirst();

    int getSize();

    void makeEmpty();

    bool checkEmpty();

    int getStart();

    int getEnd();

    class Iterator {
    private:
        int elementPointer_;
        Queue *queuePointer_;
    public:
        Iterator(Queue &queue);

        void start(); // начать перебор элементов
        void next(); // перейти к следующему элементу
        bool finish(); // проверка, все ли проитерировано
        int getValue(); // получить очередной элемент очереди
        int checkPlacement();
    };
};


#endif //INC_2_PAGE_QUEUE_H
