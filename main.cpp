#include <iostream>
#include "Queue.h"
#include "QueueExceptions.h"

using namespace std;

int main() {
    Queue qu(3);
    Queue::Iterator iterator(qu);

    cout << "Initial conditions: Start index " << qu.getStart() << "  End index " << qu.getEnd() << "\n";
    qu.add(1);
    qu.add(2);
    qu.add(3);
    cout << "Three elements added: Start index " << qu.getStart() << "  End index " << qu.getEnd() << "\n" << endl;

    try {
        qu.add(5);
    }
    catch (QueueException *e) {
        cout << e->getMessage() << "\n" << endl;
    }

    qu.remove();
    qu.remove();
    qu.remove();
    cout << "Three elements removed: Start index " << qu.getStart() << "  End index " << qu.getEnd() << "\n" << endl;

    try {
        qu.remove();
    }
    catch (QueueException *e) {
        cout << e->getMessage() << "\n" << endl;

    }
    cout << "An element was added" << endl;
    qu.add(2);

    cout << "Current first element is -- <" << qu.getFirst() << ">\n";
    cout << "Current amount of elements -- " << qu.getSize() << "\n";
    cout << "One element in queue: Start index " << qu.getStart() << "  End index " << qu.getEnd() << "\n" << endl;

    cout << "The queue was cleaned" << endl;
    qu.makeEmpty();

    if (qu.checkEmpty())
        cout << "Queue is empty\n";
    else
        cout << "Queue contains elements\n";

    cout << "\nAdd and remove some new elements" << endl;
    qu.add(6);
    qu.add(7);
    qu.add(8);
    qu.remove();
    qu.remove();
    qu.add(9);
    qu.add(10);
    cout << "Queue start is at -- " << qu.getStart() << "\n" << endl;

    iterator.start();
    cout << "Iterator's been called. Position is -- " << iterator.checkPlacement() << endl;
    iterator.next();
    cout << "Iterator's been moved forward. Position is -- " << iterator.checkPlacement() << endl;
    iterator.next();
    cout << "Iterator's reached last element. Position is -- " << iterator.checkPlacement() << endl;
    if (iterator.finish()) {
        cout << "Iterator's reached every element. Last element is <" << iterator.getValue() << ">\n";
    } else {
        cout << "There are some other elements\n";
    }
    qu.remove();
    qu.remove();
    qu.remove();
try{
    iterator.getValue();
}
catch (logic_error &error) {
    cerr << error.what() << endl;
}
    return 0;
}
