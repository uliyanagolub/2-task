//
// Created by E540 on 03/06/2020.
//

#include "Queue.h"
#include <stdexcept>


Queue::Iterator::Iterator(Queue &queue) {
    elementPointer_ = -1;
    queuePointer_ = &queue;
}

void Queue::Iterator::start() {    // начать перебор элементов
    elementPointer_ = queuePointer_->start_;
}

void Queue::Iterator::next() {    // перейти к следующему элементу
    if (elementPointer_ == queuePointer_->size_ - 1) {
        elementPointer_ = 0;
    } else {
        elementPointer_++;
    }
}

bool Queue::Iterator::finish() {    // проверка, все ли проитерировано
    return elementPointer_ == queuePointer_->end_;
}

int Queue::Iterator::getValue() {    // получить очередной элемент очереди

    if (elementPointer_ != -1) {
        if (queuePointer_->buffer_[elementPointer_] != nullptr)
            return (*queuePointer_->buffer_[elementPointer_]);
        else
            throw logic_error("Iterator locates out of queue. Restart iteration");
    }
    throw logic_error("Iteration hasn't been initialized");
}

int Queue::Iterator::checkPlacement() {
    return elementPointer_;
}

