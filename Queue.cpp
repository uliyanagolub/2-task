//
// Created by E540 on 01/06/2020.
//

#include "Queue.h"
#include "QueueExceptions.h"

Queue::Queue(int size)
{
    size_ = size;
    start_ = end_ = -1;
    buffer_ = new int*[size];
}

void Queue::add(int element){
    if ((start_ == 0 && end_ == size_-1) || (end_ == (start_-1)%(size_-1)))
        throw new QueueException("Exception! Queue is Full");

    else if (start_ == -1)
    {
        start_ = end_ = 0;
        buffer_[end_] = new int(element);
    }

    else if (end_ == size_-1 && start_ != 0)
    {
        end_ = 0;
        buffer_[end_] = new int(element);
    }

    else
    {
        end_++;
        buffer_[end_] = new int(element);
    }

}

int Queue::remove(){

    if (start_ == -1)
    {
        throw new QueueException("Exception! Queue is Empty");
    }

    int data = *buffer_[start_];
    buffer_[start_] = nullptr;

    if (start_ == end_)
    {
        start_ = -1;
        end_ = -1;
    }
    else if (start_ == size_-1)
        start_ = 0;
    else
        start_++;

    return data;

}

int Queue::getFirst()
{
    if (start_ == -1 && end_ == -1)
        return NULL;

    return *buffer_[start_];
}

int Queue::getSize()
{
    if (end_ == -1 && start_ == -1)
        return 0;

    if (end_ >= start_)
        return end_ - start_ + 1;
    else
        return size_ - start_ + end_ + 1;
}

void Queue::makeEmpty()
{
    if (end_ >= start_) {
        for (int i = start_; i <= end_; i++)
            buffer_[i] = nullptr;
        end_ = start_ = -1;
    }
}

int Queue::getStart(){
    return start_;
}
int Queue::getEnd(){
    return end_;
}


bool Queue::checkEmpty()
{
    return end_ == -1 && start_ == -1;
};
