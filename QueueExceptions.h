//
// Created by E540 on 01/06/2020.
//

#ifndef INC_2_PAGE_QUEUEEXCEPTIONS_H
#define INC_2_PAGE_QUEUEEXCEPTIONS_H

#include <string>

using namespace std;

class QueueException {

private:
    string message_;

public:
    explicit QueueException(string message);

    string getMessage();

};


#endif //INC_2_PAGE_QUEUEEXCEPTIONS_H
